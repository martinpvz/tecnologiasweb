<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN”
“http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title> Manejo de Variables </title>
<link rel="stylesheet" href="./styles.css">
</head>
<body>
  <h1>Práctica 03 - Manejo de Variables</h1>
  <h2>1. Determina cuál de las siguientes variables son válidas y explica por qué:</h2>
  <?php
  $_myvar = 0;
  $_7var = 0;
  // myvar = 0;
  $myvar = 0;
  $var7 = 0;
  $_element1 = 0;
  // $house*5 = 0;
  ?>
  <ul>
    <li>$_myvar es válida porque inicia con guión bajo.</li>
    <li>$_7var es válida porque inicia con guión bajo.</li>
    <li>myvar no es válida porque no comienza con el símbolo $.</li>
    <li>$myvar es válida porque inicia con una letra.</li>
    <li>$var7 es válida porque usa letras y números.</li>
    <li>$_element1 es válida porque inicia con guión bajo.</li>
    <li>$house*5 no es válida porque usa un caracter no permitido.</li>
  </ul>
  <hr>
  <h2>2. Proporcionar los valores de $a, $b y $c como sigue:</h2>
  <?php
  $a = "ManejadorSQL";
  $b = 'MySQL';
  $c = &$a;
  echo "<h3>A, B y C antes de modificar:</h3>";
  echo '<ul><li>'. $a . '</li><li>'. $b . '</li><li>'. $c . '</li></ul>';
  $a = "PHP server";
  $b = &$a;
  echo "<h3>A, B y C después de modificar:</h3>";
  echo '<ul><li>'. $a . '</li><li>'. $b . '</li><li>'. $c . '</li></ul>';
  unset($a, $b, $c);
  ?>
  <p>Se puede observar que al modificar la variable $a por "PHP server", la variable $c también se modifica, sin embargo asignamos a $b el valor de &$a, lo que provoca que de igual forma apunte al valor de $a y que al final la impresión de los tres valores sea "PHP server".</p>
  <hr>
  <h2>3. Muestra el contenido de cada variable inmediatamente después de cada asignación, verificar la evolución del tipo de estas variables (imprime todos los componentes de los arreglo):</h2>
  <?php
  error_reporting(E_ERROR);
  $a = "PHP5";
  echo "<p>". $a ." -------------- ". gettype($a) ."</p>";
  $z[] = &$a;
  print_r($z);
  echo " -------------- ". gettype($z);
  $b = "5a version de PHP";
  echo "<p>". $b ." -------------- ". gettype($b) ."</p>";
  $c = $b*10;
  echo "<p>". $c ." -------------- ". gettype($c) ."</p>";
  $a .= $b;
  echo "<p>". $a ." -------------- ". gettype($a) ."</p>";
  $b *= $c;
  echo "<p>". $b ." -------------- ". gettype($b) ."</p>";
  $z[0] = "MySQL";
  print_r($z);
  echo " -------------- ". gettype($z);
  ?>
  <hr>
  <h2>4. Lee y muestra los valores de las variables del ejercicio anterior, pero ahora con la ayuda de la matriz $GLOBALS o del modificador global de PHP.</h2>
  <?php
  echo '<p>El valor de $a con la matriz $GLOBALS es: <b>'. $GLOBALS['a'] .'</b></p>';
  echo '<p>El valor de $b con la matriz $GLOBALS es: <b>'. $GLOBALS['b'] .'</b></p>';
  echo '<p>El valor de $c con la matriz $GLOBALS es: <b>'. $GLOBALS['c'] .'</b></p>';
  echo '<p>El valor de $z con la matriz $GLOBALS es:<b> ';
  print_r($GLOBALS['z']);
  echo "</b></p>";

  unset($a, $b, $c, $z);
  ?>
  <hr>
  <h2>5. Dar el valor de las variables $a, $b, $c al final del siguiente script:</h2>
  <?php
  $a = "7 personas";
  $b = (integer) $a;
  $a = "9E3";
  $c = (double) $a;
  echo '<ul><li>'. $a . '</li><li>'. $b . '</li><li>'. $c . '</li></ul>';

  unset($a, $b, $c);
  ?>
  <hr>
  <h2>6. Dar y comprobar el valor booleano de las variables $a, $b, $c, $d, $e y $f y muéstralas usando la función var_dump(). Después investiga una función de PHP que permita transformar el valor booleano de $c y $e en uno que se pueda mostrar con un echo:</h2>
  <?php
  $a = "0"; $b = "TRUE"; $c = FALSE; $d = ($a OR $b); $e = ($a AND $c); $f = ($a XOR $b);
  function isBool($variable) {
    if(is_bool($variable)) {
      echo "<p>Es booleano</p>";
    }
    else {
      echo "<p>No es booleano</p>";
    }
  }
  isBool($a);
  isBool($b);
  isBool($c);
  isBool($d);
  isBool($e);
  isBool($f);
  echo "<h3>Impresión con var_dump:</h3>";
  var_dump($a, $b, $c, $d, $e, $f);
  echo '<h3>Impresión de variable $c y $e:</h3>';
  $c = var_export($c, true);
  echo "<p>Valor de C:<b> $c</b></p>";
  $e = var_export($e, true);
  echo "<p>Valor de E:<b> $e</b></p>";
  ?>
  <hr>
  <h2>7. Usando la variable predefinida $_SERVER, determina lo siguiente:</h2>
  <?php
  echo  "<p>"."a. La versión de Apache y PHP: <b>". $_SERVER['SERVER_SOFTWARE']. "</b></p>";
  echo  "<p>"."b. El nombre del sistema operativo (servidor):<b> ". $_SERVER['HTTP_USER_AGENT']. "</b></p>";
  echo  "<p>"."c. El idioma del navegador (cliente):<b> ". substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2). "</b></p>";
  ?>
</body>
</html>